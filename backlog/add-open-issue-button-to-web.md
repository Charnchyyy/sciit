---
@issue add-open-issue-button-to-web
@title Add Open Issue Button to Web Interface
@description 
 Be able to create new markdown issues in the backlog directory 
 directly from the user interface on the web application.
 This would include the ability to set all of the expected fields
 such as priority, milestone and so on for an issue tracker and 
 would then generate an issue with an appropriate slug id and
 markdown filename in backlog. 
---
