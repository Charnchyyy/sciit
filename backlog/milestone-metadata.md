---
@issue milestone-metadata
@title Milestone Metadata Item
@description
 Milestones are an important way of clustering
 new features in an issue tracker.  This feature
 Requires a new meta-data item to record the 
 milestone for an issue and a switch in the tracker
 command to sort issues by milstone.  
---

