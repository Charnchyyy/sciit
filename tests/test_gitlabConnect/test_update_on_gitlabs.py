import unittest
from git import Repo
from unittest.mock import patch
from sciit.gitlabConnect.update_on_gitlabs import update_issue_on_gitlab, print_summary
from tests.test_gitlabConnect.external_resources import *


@patch('sciit.gitlabConnect.update_on_gitlabs.print_summary')
@patch('sciit.gitlabConnect.update_on_gitlabs.GitlabRepo')
class TestGitlabConnectUpdateOnGitlab(unittest.TestCase):

    def setup(self):
        pass

    def test_update_on_gitlab_given_no_issue(self, gitlab_repo_mock, print_summary_mock):
        gitlab_repo = gitlab_repo_mock.return_value
        gitlab_repo.is_functional.return_value = True
        gitlab_repo.get_gitlab_communicator.return_value = generate_gitlab_communicator()
        gitlab_repo.retrieve_all_closed_id_from_db.return_value = []
        gitlab_repo.get_gitlab_id_from_issue_id = lambda issue: issue.id

        git_repository = Repo(search_parent_directories=True)
        update_issue_on_gitlab(git_repository, {})

        print_summary_mock.assert_called_once_with([], [], [])

    def test_update_on_gitlab_given_gitlab_repo_is_not_functional(self, gitlab_repo_mock, print_summary_mock):
        gitlab_repo = gitlab_repo_mock.return_value
        gitlab_repo.is_functional.return_value = False

        git_repository = Repo(search_parent_directories=True)
        update_issue_on_gitlab(git_repository, {})

        gitlab_repo.print_uninitialized_error.assert_called_once()
        print_summary_mock.assert_not_called()

    def test_update_on_gitlab_given_no_changes(self, gitlab_repo_mock, print_summary_mock):
        sciit_issue = generate_sciit_issue(generate_default_sciit_issue_data())
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        all_gitlab_issues = {gitlab_issue.iid: gitlab_issue}

        gitlab_repo = gitlab_repo_mock.return_value
        gitlab_repo.is_functional.return_value = True
        gitlab_repo.get_gitlab_communicator.return_value = generate_gitlab_communicator(all_gitlab_issues)
        gitlab_repo.retrieve_all_closed_id_from_db.return_value = []
        gitlab_repo.get_gitlab_id_from_issue_id = lambda id: id

        git_repository = Repo(search_parent_directories=True)
        update_issue_on_gitlab(git_repository, {sciit_issue.id: sciit_issue, })

        print_summary_mock.assert_called_once_with([], [], [])

    def test_update_on_gitlab_given_need_to_create(self, gitlab_repo_mock, print_summary_mock):
        sciit_issue = generate_sciit_issue(generate_default_sciit_issue_data())
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        all_gitlab_issues = {gitlab_issue.iid: gitlab_issue}

        gitlab_repo = gitlab_repo_mock.return_value
        gitlab_repo.is_functional.return_value = True
        gitlab_repo.get_gitlab_communicator.return_value = generate_gitlab_communicator(all_gitlab_issues)
        gitlab_repo.retrieve_all_closed_id_from_db.return_value = []
        gitlab_repo.get_gitlab_id_from_issue_id = lambda id: None


        git_repository = Repo(search_parent_directories=True)
        update_issue_on_gitlab(git_repository, {sciit_issue.id: sciit_issue})

        gitlab_repo.insert_gitlab_id_mapping.assert_called_once()
        print_summary_mock.assert_called_once_with(['1'], [], [])
        gitlab_repo.close_issue.assert_not_called()

    def test_update_on_gitlab_given_need_to_update(self, gitlab_repo_mock, print_summary_mock):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['description'] = 'new description'
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        all_gitlab_issues = {gitlab_issue.iid: gitlab_issue}

        gitlab_repo = gitlab_repo_mock.return_value
        gitlab_repo.is_functional.return_value = True
        gitlab_repo.get_gitlab_communicator.return_value = generate_gitlab_communicator(all_gitlab_issues)
        gitlab_repo.retrieve_all_closed_id_from_db.return_value = []
        gitlab_repo.get_gitlab_id_from_issue_id = lambda id: id

        git_repository = Repo(search_parent_directories=True)
        update_issue_on_gitlab(git_repository, {sciit_issue.id: sciit_issue})

        gitlab_repo.insert_gitlab_id_mapping.assert_not_called()
        print_summary_mock.assert_called_once_with([], ['1'], [])
        gitlab_repo.close_issue.assert_not_called()

    def test_update_on_gitlab_given_need_to_close(self, gitlab_repo_mock, print_summary_mock):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['status'] = 'Close'
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        all_gitlab_issues = {gitlab_issue.iid: gitlab_issue}

        gitlab_repo = gitlab_repo_mock.return_value
        gitlab_repo.is_functional.return_value = True
        gitlab_repo.get_gitlab_communicator.return_value = generate_gitlab_communicator(all_gitlab_issues)
        gitlab_repo.retrieve_all_closed_id_from_db.return_value = []
        gitlab_repo.get_gitlab_id_from_issue_id = lambda id: id

        git_repository = Repo(search_parent_directories=True)
        update_issue_on_gitlab(git_repository, {sciit_issue.id: sciit_issue})

        gitlab_repo.insert_gitlab_id_mapping.assert_not_called()
        print_summary_mock.assert_called_once_with([], [], ['1'])
        gitlab_repo.close_issue.assert_called_once()

    def test_update_on_gitlab_given_already_close(self, gitlab_repo_mock, print_summary_mock):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['status'] = 'Close'
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        all_gitlab_issues = {gitlab_issue.iid: gitlab_issue}

        gitlab_repo = gitlab_repo_mock.return_value
        gitlab_repo.is_functional.return_value = True
        gitlab_repo.get_gitlab_communicator.return_value = generate_gitlab_communicator(all_gitlab_issues)
        gitlab_repo.retrieve_all_closed_id_from_db.return_value = [sciit_issue.id]
        gitlab_repo.get_gitlab_id_from_issue_id = lambda id: id

        git_repository = Repo(search_parent_directories=True)
        update_issue_on_gitlab(git_repository, {sciit_issue.id: sciit_issue})

        gitlab_repo.insert_gitlab_id_mapping.assert_not_called()
        print_summary_mock.assert_called_once_with([], [], [])
        gitlab_repo.close_issue.assert_not_called()


@patch('sciit.gitlabConnect.update_on_gitlabs.ColorPrint')
class TestPrintSummary(unittest.TestCase):

    def test_print_no_changes(self, color_print_mock):
        print_summary([], [], [])
        color_print_mock.bold.assert_called_once()
        color_print_mock.green.assert_not_called()
        color_print_mock.yellow.assert_not_called()
        color_print_mock.red.assert_not_called()
        color_print_mock.bold_green.assert_called_once()

    def test_print_create(self, color_print_mock):
        print_summary(['1'], [], [])
        color_print_mock.bold.assert_called_once()
        color_print_mock.green.assert_called_once()
        color_print_mock.yellow.assert_not_called()
        color_print_mock.red.assert_not_called()
        color_print_mock.bold_green.assert_not_called()

    def test_print_update(self, color_print_mock):
        print_summary([], ['1'], [])
        color_print_mock.bold.assert_called_once()
        color_print_mock.green.assert_not_called()
        color_print_mock.yellow.assert_called_once()
        color_print_mock.red.assert_not_called()
        color_print_mock.bold_green.assert_not_called()

    def test_print_create(self, color_print_mock):
        print_summary([], [], ['1'])
        color_print_mock.bold.assert_called_once()
        color_print_mock.green.assert_not_called()
        color_print_mock.yellow.assert_not_called()
        color_print_mock.red.assert_called_once()
        color_print_mock.bold_green.assert_not_called()

    def test_print_all(self, color_print_mock):
        print_summary(['1'], ['1'], ['1'])
        color_print_mock.bold.assert_called_once()
        color_print_mock.green.assert_called_once()
        color_print_mock.yellow.assert_called_once()
        color_print_mock.red.assert_called_once()
        color_print_mock.bold_green.assert_not_called()


if __name__ == '__main__':
    unittest.main()
