from unittest.mock import Mock


def generate_default_sciit_issue_data():
    data = dict()
    data['id'] = '1'
    data['issue_id'] = '1'
    data['title'] = 'title'
    data['description'] = 'description'
    data['due_date'] = '10/10/2018'
    data['priority'] = 'high'
    data['label'] = 'label1,label2'
    data['status'] = ('Open', 'In progess')
    data['file_path'] = 'test.py'
    return data


def generate_default_gitlab_issue_data():
    data = dict()
    data['iid'] = '1'
    data['title'] = 'title'
    data['description'] = 'description'
    data['due_date'] = '10/10/2018'
    data['priority'] = 'high'
    data['labels'] = ['high-priority', 'label1', 'label2']
    data['state'] = 'open'
    return data


def generate_sciit_issue(data):
    issue = Mock()
    issue.id = data['id'] if 'id' in data else None
    issue.issue_id = data['issue_id'] if 'issue_id' in data else None
    issue.title = data['title'] if 'title' in data else None
    issue.description = data['description'] if 'description' in data else None
    issue.due_date = data['due_date'] if 'due_date' in data else None
    issue.priority = data['priority'] if 'priority' in data else None
    issue.label = data['label'] if 'label' in data else None
    issue.status = data['status'] if 'status' in data else None
    issue.assignees = data['assignees'] if 'assignees' in data else None
    issue.file_path = data['file_path'] if 'file_path' in data else None
    return issue


def generate_gitlab_issue(data):
    issue = Mock()
    issue.iid = data['iid'] if 'iid' in data else None
    issue.title = data['title'] if 'title' in data else None
    issue.description = data['description'] if 'description' in data else None
    issue.due_date = data['due_date'] if 'due_date' in data else None
    issue.labels = data['labels'] if 'labels' in data else []
    issue.state = data['state'] if 'state' in data else None
    issue.assignee = data['assignee'] if 'assignees' in data else None

    return issue


def generate_gitlab_communicator(issues_dict={}):
    glc = Mock()
    glc.fetch_issues = lambda: issues_dict
    glc.fetch_open_issues = lambda: issues_dict
    glc.create_issue = lambda issue: generate_gitlab_issue({'iid': issue.id})
    return glc
