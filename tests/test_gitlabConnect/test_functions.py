import unittest
from unittest.mock import patch
from sciit.gitlabConnect.functions import *
from tests.test_gitlabConnect.external_resources import *


class TestGetProjectUrl(unittest.TestCase):

    def test_get_project_url_given_clone_from_ssh(self):
        git_repo = Mock()
        git_repo.git.remote = lambda *args: "git@gitlab.com:Charnchyyy/test.git"

        website_url, project_url = get_project_url(git_repo)

        self.assertEqual('https://gitlab.com', website_url)
        self.assertEqual('Charnchyyy/test', project_url)

    def test_get_project_url_given_clone_from_https(self):
        git_repo = Mock()
        git_repo.git.remote = lambda *args: "https://gitlab.com/Charnchyyy/test.git"

        website_url, project_url = get_project_url(git_repo)

        self.assertEqual('https://gitlab.com', website_url)
        self.assertEqual('Charnchyyy/test', project_url)


class TestGetPriorityLabel(unittest.TestCase):

    def test_get_priority_label_high(self):
        priority = 'high'
        priority_label = get_priority_label(priority)
        self.assertEqual('high-priority', priority_label)

    def test_get_priority_label_high_upper_case(self):
        priority = 'HIGH'
        priority_label = get_priority_label(priority)
        self.assertEqual('high-priority', priority_label)

    def test_get_priority_label_medium(self):
        priority = 'medium'
        priority_label = get_priority_label(priority)
        self.assertEqual('medium-priority', priority_label)

    def test_get_priority_label_medium_upper_class(self):
        priority = 'MEDIUM'
        priority_label = get_priority_label(priority)
        self.assertEqual('medium-priority', priority_label)

    def test_get_priority_label_low(self):
        priority = 'low'
        priority_label = get_priority_label(priority)
        self.assertEqual('low-priority', priority_label)

    def test_get_priority_label_low_upper_class(self):
        priority = 'LOW'
        priority_label = get_priority_label(priority)
        self.assertEqual('low-priority', priority_label)

    def test_get_priority_label_random_string(self):
        priority = 'lavel'
        priority_label = get_priority_label(priority)
        self.assertEqual(None, priority_label)


class TestExtractPriorityFromLabel(unittest.TestCase):

    def test_extract_priority_from_label_given_low_priority_in_the_list(self):
        label_list = ['test1', 'test2', 'low-priority', 'test3']
        priority, new_label_list = extract_priority_from_label(label_list)
        self.assertEqual('low', priority)
        self.assertEqual(['test1', 'test2', 'test3'], new_label_list)

    def test_extract_priority_from_label_given_medium_priority_in_the_list(self):
        label_list = ['test1', 'test2', 'medium-priority', 'test3']
        priority, new_label_list = extract_priority_from_label(label_list)
        self.assertEqual('medium', priority)
        self.assertEqual(['test1', 'test2', 'test3'], new_label_list)

    def test_extract_priority_from_label_given_high_priority_in_the_list(self):
        label_list = ['test1', 'test2', 'high-priority', 'test3']
        priority, new_label_list = extract_priority_from_label(label_list)
        self.assertEqual('high', priority)
        self.assertEqual(['test1', 'test2', 'test3'], new_label_list)

    def test_extract_priority_from_label_given_None(self):
        priority, new_label_list = extract_priority_from_label(None)
        self.assertEqual(None, priority)
        self.assertEqual([], new_label_list)


class TestCompareIssueWithGitlabIssue(unittest.TestCase):

    def test_compare_issue_wth_gitlab_issue_given_similar_issues(self):
        sciit_data = generate_default_sciit_issue_data()
        gitlab_data = generate_default_gitlab_issue_data()
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertTrue(same)

    def test_compare_issue_wth_gitlab_issue_given_different_title(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['title'] = 'new title'
        gitlab_data = generate_default_gitlab_issue_data()
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertFalse(same)

    def test_compare_issue_wth_gitlab_issue_given_different_description(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['description'] = 'new description'
        gitlab_data = generate_default_gitlab_issue_data()
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertFalse(same)

    def test_compare_issue_wth_gitlab_issue_given_different_due_date(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['due_date'] = None
        gitlab_data = generate_default_gitlab_issue_data()
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertFalse(same)

    def test_compare_issue_wth_gitlab_issue_given_different_priority(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['priority'] = None
        gitlab_data = generate_default_gitlab_issue_data()
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertFalse(same)

    def test_compare_issue_wth_gitlab_issue_given_different_label(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['label'] = 'one,two,three'
        gitlab_data = generate_default_gitlab_issue_data()
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertFalse(same)

    def test_compare_issue_wth_gitlab_issue_given_one_label_is_none(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['label'] = None
        gitlab_data = generate_default_gitlab_issue_data()
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertFalse(same)

    def test_compare_issue_wth_gitlab_issue_given_both_labels_are_none(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['label'] = None
        sciit_data['priority'] = 'high'
        gitlab_data = generate_default_gitlab_issue_data()
        gitlab_data['labels'] = ['high-priority']
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(gitlab_data)
        same = compare_issue_wth_gitlab_issue(sciit_issue, gitlab_issue)
        self.assertTrue(same)


@patch('sciit.gitlabConnect.functions.get_comment_style_from_file_extension')
class TestFormatIssueForSourceCode(unittest.TestCase):

    def test_format_issue_for_source_code(self, comment_style):
        comment_style.return_value = '"""', '    ', '"""'
        issue_data = generate_default_sciit_issue_data()
        expected_ans = ('"""\n' +
                        '     @issue: 1\n'
                        '     @title: title\n' +
                        '     @description:\n'
                        '     description\n' +
                        '     @due date: 10/10/2018\n' +
                        '     @label: label1,label2\n' +
                        '     @priority: high\n' +
                        '"""\n')
        formatted_string = format_issue_for_source_code(issue_data)
        self.assertEqual(expected_ans, formatted_string)


class TestConvertIssuesToDict(unittest.TestCase):

    def test_covert_issue_to_dict_given_normal_case(self):
        sciit_issue_data = generate_default_sciit_issue_data()
        sciit_issue = generate_sciit_issue(sciit_issue_data)
        gitlab_issue_data = generate_default_gitlab_issue_data()
        gitlab_issue = generate_gitlab_issue(gitlab_issue_data)
        priority, labels = extract_priority_from_label(gitlab_issue.labels)
        labels = ", ".join(labels)

        result = convert_issue_to_dict(gitlab_issue, sciit_issue)

        self.assertEqual(dict, type(result))
        self.assertEqual(sciit_issue.id, result['issue_id'])
        self.assertEqual(sciit_issue.file_path, result['file_path'])
        self.assertEqual(gitlab_issue.title, result['title']),
        self.assertEqual(gitlab_issue.description, result['description'].strip())
        self.assertEqual(gitlab_issue.due_date, result['due_date'])
        self.assertEqual(priority, result['priority'])
        self.assertEqual(labels, result['label'])

    def test_covert_issue_to_dict_given_default_case(self):
        gitlab_issue_data = generate_default_gitlab_issue_data()
        gitlab_issue = generate_gitlab_issue(gitlab_issue_data)
        priority, labels = extract_priority_from_label(gitlab_issue.labels)
        labels = ", ".join(labels)

        result = convert_issue_to_dict(gitlab_issue)

        self.assertEqual(dict, type(result))
        self.assertEqual(f'{gitlab_issue.iid}-{gitlab_issue.title}', result['issue_id'])
        self.assertEqual(f'{gitlab_issue.iid}-{gitlab_issue.title}.md', result['file_path'])
        self.assertEqual(gitlab_issue.title, result['title']),
        self.assertEqual(gitlab_issue.description, result['description'].strip())
        self.assertEqual(gitlab_issue.due_date, result['due_date'])
        self.assertEqual(priority, result['priority'])
        self.assertEqual(labels, result['label'])


if __name__ == '__main__':
    unittest.main()
