import unittest
from unittest.mock import patch
from sciit.gitlabConnect.update_source_code import *
from sciit.gitlabConnect.functions import extract_priority_from_label, convert_issue_to_dict, \
    format_issue_for_source_code
from tests.test_gitlabConnect.external_resources import *


class TestEditString(unittest.TestCase):

    def test_edit_string_given_normal_case(self):
        original_string = '012345'
        add_string = 'x'
        new_string = edit_string(original_string, add_string, 2, 4)
        self.assertEqual('01x45', new_string)

    def test_edit_string_given_end_index_is_more_than_length(self):
        original_string = '012345'
        add_string = 'x'
        new_string = edit_string(original_string, add_string, 2, 400)
        self.assertEqual('01x', new_string)

    def test_edit_string_given_invalid_parameter(self):
        original_string = '012345'
        add_string = 'x'
        self.assertRaises(AttributeError, edit_string, original_string, add_string, 20, 4)
        self.assertRaises(AttributeError, edit_string, original_string, add_string, 20, 25)


if __name__ == '__main__':
    unittest.main()
