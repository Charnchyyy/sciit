import unittest
from sciit.gitlabConnect.issue_matching import match_gitlab_issue_with_sciit, match_sciit_with_gitlab_issue
from tests.test_gitlabConnect.external_resources import *


class TestMatchGitlabIssueWithSciit(unittest.TestCase):

    def setup(self):
        pass

    def test_match_gitlab_issue_with_sciit_found(self):
        sciit_issue = generate_sciit_issue(generate_default_sciit_issue_data())
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        sciit_issue_dict = dict()
        sciit_issue_dict[sciit_issue.issue_id] = sciit_issue

        found_id = match_gitlab_issue_with_sciit(gitlab_issue, sciit_issue_dict)

        self.assertEqual(sciit_issue.issue_id, found_id)

    def test_match_gitlab_issue_with_sciit_not_found(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['title'] = "not the same"
        sciit_data['description'] = "still not the same"
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        sciit_issue_dict = dict()
        sciit_issue_dict[sciit_issue.issue_id] = sciit_issue

        found_id = match_gitlab_issue_with_sciit(gitlab_issue, sciit_issue_dict)

        self.assertEqual(None, found_id)


class TestMatchSciitIssueWithGitLab(unittest.TestCase):

    def setup(self):
        pass

    def test_match_sciit_with_gitlab_issue_found(self):
        sciit_issue = generate_sciit_issue(generate_default_sciit_issue_data())
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        gitlab_issue_dict = dict()
        gitlab_issue_dict[gitlab_issue.id] = gitlab_issue

        found_id = match_sciit_with_gitlab_issue(sciit_issue, gitlab_issue_dict)

        self.assertEqual(gitlab_issue.id, found_id)

    def test_match_sciit_with_gitlab_issue_not_found(self):
        sciit_data = generate_default_sciit_issue_data()
        sciit_data['title'] = "not the same"
        sciit_data['description'] = "still not the same"
        sciit_issue = generate_sciit_issue(sciit_data)
        gitlab_issue = generate_gitlab_issue(generate_default_gitlab_issue_data())
        gitlab_issue_dict = dict()
        gitlab_issue_dict[gitlab_issue.id] = gitlab_issue

        found_id = match_sciit_with_gitlab_issue(sciit_issue, gitlab_issue_dict)

        self.assertEqual(None, found_id)


if __name__ == '__main__':
    unittest.main()
