# Command Line Usage

```bash
git-sciit | git sciit | git-sciit.exe <command> <options>
```

## Init

`git sciit init [-r | --reset]

Creates an empty repository or builds from past commits.

`[--reset | -r]` Removes all existing issue repository artifacts, including git hooks and cached issue snapshot database before rebulding.

## Status

`git sciit status [-f | -n] [revision]

Shows the user how many issues are open and how many are closed on all branches.

`[--full | -f]` Shows a table of open and closed issues.
`[--normal | -n]` Shows a count of open and closed issues.

## Log

`git sciit log [revision]`

Outputs a log that is similar to the git command, but includes a summary of open issues for each commit.

`[revision]` The git revision path to use to control logging.

## Issue

`git sciit issue [-f | -n ] issueid [revision]`

Shows information about the issue with the given id.

`[--full | -f]` Shows the full history of changes to the issue.
`[--normal | -n]` Shows the normal summary of the current state of the issue.

## Web

`git sciit web`

Launches the web interface for viewing issue information.

## Tracker

`git sciit tracker [-a | -o | -c ] [-f | -n ]  [revision]`

`[--full | -f]` Shows the full history of changes to the issues.
`[--normal | -n]` Shows the normal summary of the current state of the issues.

`[revision]` The git revision path to use to control the view of the issues.

## Gitlab-init

`git sciit gitlab-init [-h] [-r] [-m] token`
Use to initialized connection with Gitlab issue tracker

token Stores user's [Gitlab private token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
`[--reset | -r]` Removes all existing issue repository artifacts, including git hooks and cached issue snapshot database before rebulding.
`[--migrate | -m]` Migrates all existed issues from Gitlab into SCIIT

## Gitlab 

`git sciit gitlab [-h] {migrate,pull,push}`

Usage:

`git sciit gitlab [-h] migrate` Migrates all existed issues from Gitlab into SCIIT.

`git sciit gitlab [-h] pull` Update SCIIT issues one issue on Gitlab had been edited.

`git sciit gitlab [-h] push` Manually update issues on gitlab issues tracker.

`git sciit gitlab [-h] disconnect` Disconnect gitlab issue tracker integration.