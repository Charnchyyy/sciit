# -*- coding: utf-8 -*-

import os
import sqlite3
import stat
from contextlib import closing

from sciit.errors import EmptyRepositoryError
from sciit.gitlabConnect.functions import get_project_url
from sciit.gitlabConnect.gitlabCommunicator import GitlabCommunicator
from sciit.cli.color import ColorPrint
import pkg_resources

from shutil import copyfile

__all__ = ('GitlabRepo',)


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class GitlabRepo(object):

    def __init__(self, git_repository):
        self.git_repository = git_repository
        self.gitlab_dir = self.git_repository.git_dir + '/gitlab'
        self.mapping_db = '/gitlab.db'
        self.gitlab_id_mapping_cache = dict()

        self.cli = False

    def is_init(self):
        return os.path.exists(self.gitlab_dir)

    def is_functional(self):
        if not self.is_init():
            return False
        if self.get_gitlab_token() is None:
            return False
        return True

    def print_uninitialized_error(self):
        ColorPrint.red(
            "The Gitlab Connection have not been initialized run \'git sciit gitlab-init [YOUR_TOKEN]\'")

    def setup_file_system_resources(self):
        os.makedirs(self.gitlab_dir)
        self.store_gitlab_project_meta_data()
        self._install_hook('pre-push')

    def _install_hook(self, hook_name):
        git_hooks_dir = self.git_repository.git_dir + '/hooks/'
        if not os.path.exists(git_hooks_dir):
            os.makedirs(git_hooks_dir)

        source_resource = pkg_resources.resource_filename('sciit.hooks', hook_name)
        destination_path = git_hooks_dir + hook_name
        copyfile(source_resource, destination_path)
        st = os.stat(destination_path)
        os.chmod(destination_path, st.st_mode | stat.S_IEXEC)

    def _uninstall_hook(self, hook_name):
        git_hooks_dir = self.git_repository.git_dir + '/hooks/'
        if not os.path.exists(git_hooks_dir):
            return
        filepath = git_hooks_dir + hook_name
        if os.path.exists(filepath):
            os.remove(filepath)

    def store_gitlab_project_meta_data(self):
        website_url, project_url = get_project_url(self.git_repository)
        with open(self.gitlab_dir + '/project', 'w') as f:
            f.write(f'{website_url},{project_url}')

    def get_gitlab_project_meta_data(self):
        project_file = self.gitlab_dir + '/project'
        if not os.path.isfile(project_file):
            raise EmptyRepositoryError
        with open(project_file, 'r') as f:
            website_url, project_url = f.read().split(',')
        return website_url, project_url

    def store_gitlab_token(self, gitlab_token):
        with open(self.gitlab_dir + '/access_token', 'w') as f:
            f.write(gitlab_token)

    def get_gitlab_token(self):
        token_file = self.gitlab_dir + '/access_token'
        if not os.path.isfile(token_file):
            return None
        with open(token_file, 'r') as f:
            token = f.read()
        return token

    def get_gitlab_communicator(self):
        try:
            private_token = self.get_gitlab_token()
            website_url, project_url = self.get_gitlab_project_meta_data()
            return GitlabCommunicator(website_url, project_url, private_token)
        except EmptyRepositoryError:
            self.print_uninitialized_error()
            return None

    def reset(self):
        def onerror(func, path, excp_info):
            os.chmod(path, stat.S_IWUSR)
            func(path)

        if self.is_init():
            import shutil
            shutil.rmtree(self.gitlab_dir, onerror=onerror)
            self._uninstall_hook('pre-push')
        else:
            raise EmptyRepositoryError

    def insert_gitlab_id_mapping(self, issue_id, gitlab_id):
        self._serialize_mapping_to_id(issue_id, gitlab_id)

    def get_gitlab_id_from_issue_id(self, issue_id):
        if issue_id in self.gitlab_id_mapping_cache:
            return self.gitlab_id_mapping_cache[issue_id]
        find_from_db, _ = self._deserialize_mapping_from_db(issue_id)
        if issue_id in find_from_db:
            return find_from_db[issue_id]
        return None

    def retrieve_all_open_gitlab_id_from_db(self):
        return self._deserialize_mapping_from_db()[0]

    def retrieve_all_closed_id_from_db(self):
        return list(self._deserialize_mapping_from_db()[1].keys())

    def retrieve_all_gitlab_id_from_db(self):
        open_issues, close_issues = self._deserialize_mapping_from_db()
        return {**open_issues, **close_issues}

    def retrieve_all_reverse_gitlab_id(self):
        all_gitlab_id = self.retrieve_all_gitlab_id_from_db()
        reverse_mapping = dict()
        for key, value in all_gitlab_id.items():
            reverse_mapping[value] = key
        return reverse_mapping

    def close_issue(self, issue_id):
        self._update_closing_issue_from_db(issue_id)

    def _serialize_mapping_to_id(self, issue_id, gitlab_id):
        with closing(sqlite3.connect(self.gitlab_dir + self.mapping_db)) as connection:
            cursor = connection.cursor()
            self._create_gitlab_id_mapping_table(cursor)
            cursor.execute("INSERT INTO GITLAB_ID_MAPPING  VALUES(?, ?, ?)", (issue_id, gitlab_id, 1))
            connection.commit()
        self.gitlab_id_mapping_cache[issue_id] = gitlab_id

    def _update_closing_issue_from_db(self, issue_id):
        with closing(sqlite3.connect(self.gitlab_dir + self.mapping_db)) as connection:
            cursor = connection.cursor()
            self._create_gitlab_id_mapping_table(cursor)
            cursor.execute("UPDATE GITLAB_ID_MAPPING SET is_open = ? WHERE issue_id = ?", (0, issue_id))
            connection.commit()
        if issue_id in self.gitlab_id_mapping_cache:
            del self.gitlab_id_mapping_cache[issue_id]

    def _deserialize_mapping_from_db(self, issue_id=None):
        open_issues = dict()
        closed_issues = dict()

        with closing(sqlite3.connect(self.gitlab_dir + self.mapping_db)) as connection:
            cursor = connection.cursor()
            cursor.row_factory = dict_factory
            self._create_gitlab_id_mapping_table(cursor)
            if issue_id is None:
                row_values = cursor.execute("SELECT * FROM GITLAB_ID_MAPPING").fetchall()
            else:
                row_values = cursor.execute(
                    """
                    SELECT * FROM GITLAB_ID_MAPPING WHERE issue_id = ?
                    """, (issue_id,)).fetchall()

            for row_value in row_values:
                i_id = row_value['issue_id']
                gl_id = row_value['gitlab_id']
                if row_value['is_open'] == 1:
                    open_issues[i_id] = gl_id
                else:
                    closed_issues[i_id] = gl_id

        return open_issues, closed_issues

    @staticmethod
    def _create_gitlab_id_mapping_table(cursor):
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS GITLAB_ID_MAPPING(
             issue_id TEXT,
             gitlab_id INTEGER,
             is_open BIT,
             UNIQUE (issue_id) ON CONFLICT REPLACE
            )
            """
        )
