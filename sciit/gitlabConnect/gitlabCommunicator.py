import gitlab
from sciit.gitlabConnect.functions import extract_priority_from_label, get_priority_label
from sciit.issue import Issue


class GitlabCommunicator:
    def __init__(self, website_url, project_url: str, private_token: str):
        self.gitlab = gitlab.Gitlab(website_url, private_token=private_token)
        self.project = self.gitlab.projects.get(project_url)
        self.issues_list = {}
        self.fetch_issues()

    def fetch_issues(self):
        issues = self.project.issues.list()
        issues_list = {}
        for issue in issues:
            issues_list[issue.iid] = issue
        self.issues_list = issues_list
        return issues_list

    def fetch_open_issues(self):
        issues = self.project.issues.list(state='opened')
        issues_list = {}
        for issue in issues:
            issues_list[issue.iid] = issue
        self.issues_list = issues_list
        return issues_list

    def close_issue(self, issue_id: int):
        if issue_id in self.issues_list:
            issue = self.issues_list[issue_id]
            issue.state_event = 'close'
            issue.save()

    def create_issue(self, issue: Issue):
        if issue.label is not None:
            labels = list(issue.label.split(','))
        else:
            labels = []
        priority_label = get_priority_label(issue.priority)
        if priority_label:
            labels.append(priority_label)
        title = issue.title if issue.title is not None else issue.issue_id
        data = {
            'title': title,
            'description': issue.description,
            'labels': labels,
            'assignee_id': self.get_assignee_id(issue.assignees),
            'due_date': issue.due_date,
            'weight': issue.weight
        }
        return self.project.issues.create(data)

    def update_issue(self, issue: Issue, gitlab_mapping):
        if issue.label is not None:
            labels_list = issue.label.split(',')
        else:
            labels_list = []
        _, labels_list = extract_priority_from_label(labels_list)
        priority_label = get_priority_label(issue.priority)
        if priority_label:
            labels_list.append(priority_label)


        if gitlab_mapping in self.issues_list:
            title = issue.title if issue.title is not None else issue.issue_id,
            gitlab_issue = self.issues_list[gitlab_mapping]
            gitlab_issue.title = title
            gitlab_issue.description = issue.description
            gitlab_issue.labels = labels_list
            gitlab_issue.assignee_id = self.get_assignee_id(issue.assignees)
            gitlab_issue.due_date = issue.due_date
            gitlab_issue.weight = issue.weight
            gitlab_issue.save()
        else:
            self.create_issue(issue)
        self.fetch_issues()

    def get_users_in_project(self):
        users = self.project.users.list(as_list=False)
        project_users = dict()
        for user in users:
            project_users[user.username.lower()] = int(user.id)
        return project_users

    def get_assignee_id(self, assignees):
        if assignees is None:
            return 0
        project_users = self.get_users_in_project()
        assignees = assignees.lower()
        assignees = list(map(str.strip, assignees.split(',')))
        for user in assignees:
            if user in project_users:
                return project_users[user]
        return 0
