import os

import requests

from sciit.regex import get_comment_style_from_file_extension


def get_project_url(git_repo):
    origin_url = git_repo.git.remote('get-url', '--all', 'origin')
    origin_url_component = __cleanup_git_url(origin_url).split('/')
    protocol = __decide_protocol(origin_url_component)
    website_url = protocol + '://' + origin_url_component[1]
    project_url = origin_url_component[-2] + '/' + origin_url_component[-1]
    return website_url, project_url


def __cleanup_git_url(origin_url: str):
    remove_list = ['://', ':', '@']
    for remove_char in remove_list:
        origin_url = origin_url.replace(remove_char, '/')
    return origin_url.replace('.git', '')


def __decide_protocol(url_component):
    http = 'http'
    https = 'https'
    protocol = url_component[0]
    if http in protocol:
        return protocol
    website = http + '://' + url_component[1]
    req = requests.get(website)
    url = req.url
    if https in url:
        return https
    return http


def compare_issue_wth_gitlab_issue(issue, gitlab_issue, glc=None):
    if issue.title != gitlab_issue.title:
        issue_title = None
        gitlab_issue_title = None
        if issue.title:
            issue_title = issue.title.strip()
        if gitlab_issue.title:
            gitlab_issue_title = gitlab_issue.title.strip()
        if issue_title != gitlab_issue_title:
            return False
    if issue.description != gitlab_issue.description:
        issue_desc = None
        gitlab_issues_desc = None
        if issue.description is not None:
            issue_desc = issue.description.strip()
        if gitlab_issue.description:
            gitlab_issues_desc = gitlab_issue.description.strip()
        if issue_desc != gitlab_issues_desc:
            return False
    if issue.due_date != gitlab_issue.due_date:
        return False

    if gitlab_issue.assignee:
        if issue.assignees:
            issue_assignees = issue.assignees.lower()
            issue_assignees = list(map(str.strip, issue_assignees.split(',')))
            if gitlab_issue.assignee['username'].lower() not in issue_assignees:
                return False
    else:
        if glc and issue.assignees:
            if glc.get_assignee_id(issue.assignees) != 0:
                return False

    gitlab_issue_priority, gitlab_issue_label = extract_priority_from_label(gitlab_issue.labels)
    issue_priority = issue.priority
    if issue_priority:
        issue_priority = issue_priority.lower()
    if issue_priority != gitlab_issue_priority:
        return False
    if issue.label is not None:
        issue_label_list = sorted(map(str.strip, issue.label.split(',')))
    else:
        issue_label_list = []
    if issue_label_list != sorted(gitlab_issue_label):
        return False
    return True


def extract_priority_from_label(label_list: list):
    possible_priority_label = [
        'high-priority',
        'medium-priority',
        'low-priority'
    ]
    priority = None
    new_labels = []
    if label_list:
        new_labels = list(label_list)
        for label in possible_priority_label:
            if label in new_labels:
                priority = label.split('-')[0]
                new_labels.remove(label)
    return priority, new_labels


def get_priority_label(priority: str):
    if priority is None:
        return None
    priority = priority.lower()
    if priority == 'high':
        return 'high-priority'
    if priority == 'medium':
        return 'medium-priority'
    if priority == 'low':
        return 'low-priority'
    return None


def convert_issue_to_dict(gitlab_issue, issue=None):
    data = dict()
    if issue:
        data['file_path'] = issue.file_path
        data['issue_id'] = issue.issue_id.strip()
        if issue.weight:
            data['weight'] = issue.weight
    else:
        issue_id = f'{gitlab_issue.iid}-{gitlab_issue.title}'
        data['file_path'] = f'{issue_id}.md'
        data['issue_id'] = issue_id
    if gitlab_issue.title:
        data['title'] = gitlab_issue.title
    if gitlab_issue.description:
        data['description'] = "\n" + gitlab_issue.description
    if gitlab_issue.labels:
        priority, gitlab_issue_label = extract_priority_from_label(gitlab_issue.labels)
        if gitlab_issue_label:
            data['label'] = ", ".join(gitlab_issue_label)
        if priority:
            data['priority'] = priority

    issue_assignees = []
    if issue:
        if issue.assignees:
            issue_assignees = list(map(str.strip, issue.assignees.split(',')))
    gitlab_assignees = []
    if gitlab_issue.assignee:
        gitlab_assignees = [gitlab_issue.assignee['username'].lower()]
    if issue_assignees + gitlab_assignees:
        data['assignees'] = ", ".join(issue_assignees + gitlab_assignees)

    if gitlab_issue.due_date:
        data['due_date'] = gitlab_issue.due_date
    return data


def format_issue_for_source_code(data: dict):
    extension = os.path.splitext(data['file_path'])[1]
    comment_open_style, comment_body_style, comment_close_style = get_comment_style_from_file_extension(extension)
    formatted_str = f'{comment_open_style}\n'
    if 'issue_id' in data:
        formatted_str += f'{comment_body_style} @issue: {data["issue_id"]}\n'
    if 'title' in data:
        formatted_str += f'{comment_body_style} @title: {data["title"]}\n'
    if 'description' in data:
        formatted_str += f'{comment_body_style} @description:\n'
        description_list = data['description'].strip().split('\n')
        for desc in description_list:
            formatted_str += f'{comment_body_style} {desc}\n'
    if 'assignees' in data:
        formatted_str += f'{comment_body_style} @issue_assigned to: {data["assignees"]}\n'
    if 'due_date' in data:
        formatted_str += f'{comment_body_style} @due date: {data["due_date"]}\n'
    if 'label' in data:
        formatted_str += f'{comment_body_style} @label: {data["label"]}\n'
    if 'weight' in data:
        formatted_str += f'{comment_body_style} @weight: {data["weight"]}\n'
    if 'priority' in data:
        formatted_str += f'{comment_body_style} @priority: {data["priority"]}\n'
    formatted_str += f'{comment_close_style}\n'
    return formatted_str


def commit_files(git_repository, file_list=[], commit_message="update issues from gitlab"):
    git = git_repository.git
    if file_list:
        for file in file_list:
            git.add(file)
    git.commit('-m', commit_message)


def get_user_id():
    pass
