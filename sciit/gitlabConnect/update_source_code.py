from sciit.commit import find_issue_in_comment
from sciit.issue import Issue
from sciit.regex import get_file_extension_pattern
from sciit.gitlabConnect.functions import compare_issue_wth_gitlab_issue, convert_issue_to_dict, \
    format_issue_for_source_code, commit_files
from sciit.gitlabConnect.repo import GitlabRepo
import os
import re


def update_issue_from_gitlab(git_repository, issues_list):
    gitlab_repo = GitlabRepo(git_repository)
    if not gitlab_repo.is_functional():
        gitlab_repo.print_uninitialized_error()
        return
    glc = gitlab_repo.get_gitlab_communicator()
    gitlab_issues = glc.fetch_issues()
    files_updated = list()
    for issue_id, issue in issues_list.items():
        gitlab_issue_id = gitlab_repo.get_gitlab_id_from_issue_id(issue_id)
        if gitlab_issue_id:
            if gitlab_issue_id in gitlab_issues:
                gitlab_issue = gitlab_issues[gitlab_issue_id]
                if not compare_issue_wth_gitlab_issue(issue, gitlab_issue,glc):
                    issue_data = convert_issue_to_dict(gitlab_issue, issue)
                    issue_data_string = format_issue_for_source_code(issue_data)
                    update_issue_on_file(issue, issue_data_string)
                    files_updated.append(issue.file_path)
                    # print(f'\t\tupdating issue {issue_id}')
                if gitlab_issue.state == 'closed':
                    update_issue_on_file(issue, '')
                    files_updated.append(issue.file_path)
                    # print(f'\t\tclosing issue {issue_id}')
    if files_updated:
        commit_files(git_repository, files_updated)


def update_issue_on_file(issue: Issue, issue_data: str):
    extension = os.path.splitext(issue.file_path)[1]
    object_pattern = get_file_extension_pattern(extension)
    with open(issue.file_path, 'r') as f:
        file_content = f.read()

    for m in re.finditer(object_pattern, file_content):
        current_issue = find_issue_in_comment(m.group(0))
        if current_issue['issue_id'] == issue.issue_id:
            start_index = m.start()
            end_index = m.end()
            file_content = edit_string(file_content, issue_data, start_index, end_index)

    with open(issue.file_path, 'w') as f:
        f.write(file_content)


def edit_string(original_string, substitute_string, start_index, end_index):
    if end_index < start_index or start_index >= len(original_string):
        raise AttributeError()
    if end_index >= len(original_string):
        return original_string[:start_index] + substitute_string
    return original_string[:start_index] + substitute_string + original_string[end_index:]


