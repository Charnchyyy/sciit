import os
from sciit.repo import IssueRepo
from sciit.gitlabConnect.repo import GitlabRepo
from sciit.gitlabConnect.functions import convert_issue_to_dict, format_issue_for_source_code, commit_files
from sciit.gitlabConnect.issue_matching import match_gitlab_issue_with_sciit


def migrate(git_repository):
    gitlab_repo = GitlabRepo(git_repository)
    issue_repository = IssueRepo(git_repository)
    sciit_open_issues = issue_repository.get_open_issues()
    glc = gitlab_repo.get_gitlab_communicator()
    gitlab_issues = glc.fetch_issues()
    reverse_mapping = gitlab_repo.retrieve_all_reverse_gitlab_id()
    events = glc.project.events.list(target_type='issue', as_list=False, sort='asc')

    for event in events:
        event_type = event.action_name
        issue_id = event.target_iid

        if issue_id not in reverse_mapping:
            if issue_id in gitlab_issues:
                gitlab_issue = gitlab_issues[issue_id]
                sciit_issue_id = match_gitlab_issue_with_sciit(gitlab_issue, sciit_open_issues)
                if sciit_issue_id is not None:
                    gitlab_repo.insert_gitlab_id_mapping(sciit_issue_id, issue_id)
                    continue
                issue_data = convert_issue_to_dict(gitlab_issue)
                if event_type == 'opened':
                    new_issue_file = write_issue_to_mark_down(issue_data)
                    gitlab_repo.insert_gitlab_id_mapping(issue_data['issue_id'], issue_id)
                    commit_message = f'Open Gitlab issue - {issue_data["issue_id"]}:'
                    commit_files(git_repository, [new_issue_file], commit_message)
                elif event_type == 'closed':
                    deleted_issue_file = delete_issue_from_mark_down(issue_data)
                    gitlab_repo.close_issue(issue_data['issue_id'])
                    commit_message = f'Close Gitlab issue - {issue_data["issue_id"]}:'
                    commit_files(git_repository, [deleted_issue_file], commit_message)


def write_issue_to_mark_down(data):
    directory = "./issue_backlogs/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    formatted_string = format_issue_for_source_code(data)
    file_name = directory + data['file_path']
    with open(file_name, 'w+') as f:
        f.write(formatted_string)
    return file_name


def delete_issue_from_mark_down(data):
    directory = "./issue_backlogs/"
    file_name = directory + data['file_path']
    if os.path.isfile(file_name):
        os.remove(file_name)
    return file_name
