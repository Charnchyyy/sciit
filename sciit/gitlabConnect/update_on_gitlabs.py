from sciit.gitlabConnect.functions import compare_issue_wth_gitlab_issue
from sciit.gitlabConnect.repo import GitlabRepo
from sciit.gitlabConnect.issue_matching import match_sciit_with_gitlab_issue
from sciit.cli.color import ColorPrint


def update_issue_on_gitlab(git_repository, issues_list):
    gitlab_repo = GitlabRepo(git_repository)
    if not gitlab_repo.is_functional():
        gitlab_repo.print_uninitialized_error()
        return
    glc = gitlab_repo.get_gitlab_communicator()
    gitlab_issues = glc.fetch_issues()
    gitlab_open_issues = glc.fetch_open_issues()
    closed_issues = gitlab_repo.retrieve_all_closed_id_from_db()
    creating_issues = []
    updating_issues = []
    closing_issues = []
    for issue_id, issue in issues_list.items():
        if issue.status[0] == 'Open':
            gitlab_issue_id = gitlab_repo.get_gitlab_id_from_issue_id(issue_id)
            if not gitlab_issue_id:
                new_gitlab_issue_id = match_sciit_with_gitlab_issue(issue,gitlab_open_issues)
                if new_gitlab_issue_id is None:
                    new_gitlab_issue = glc.create_issue(issue)
                    new_gitlab_issue_id = new_gitlab_issue.iid
                gitlab_repo.insert_gitlab_id_mapping(issue_id, new_gitlab_issue_id)
                creating_issues.append(issue_id)
            else:
                gitlab_issue = gitlab_issues[gitlab_issue_id]
                if not compare_issue_wth_gitlab_issue(issue, gitlab_issue,glc):
                    glc.update_issue(issue, gitlab_issue_id)
                    updating_issues.append(issue_id)
        else:
            if issue_id not in closed_issues:
                gitlab_issue_id = gitlab_repo.get_gitlab_id_from_issue_id(issue_id)
                if gitlab_issue_id is not None:
                    closing_issues.append(issue_id)
                    glc.close_issue(gitlab_issue_id)
                    gitlab_repo.close_issue(issue_id)

    print_summary(creating_issues, updating_issues, closing_issues)


def print_summary(create, update, close):
    ColorPrint.bold("\nGitlab Issues updated:")
    if create:
        ColorPrint.green("\tCreating Issues: {0}".format(",".join(create)))
    if update:
        ColorPrint.yellow("\tUpdating Issues: {0}".format(",".join(update)))
    if close:
        ColorPrint.red("\tClosing Issues: {0}".format(",".join(close)))
    if not create and not update and not close:
        ColorPrint.bold_green("\tNo changes have been made")
    print()
