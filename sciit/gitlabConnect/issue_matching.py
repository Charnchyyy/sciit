from sciit.gitlabConnect.functions import compare_issue_wth_gitlab_issue


def match_sciit_with_gitlab_issue(issue, gitlab_issues):
    for gitlab_issue_id in gitlab_issues:
        gitlab_issue = gitlab_issues[gitlab_issue_id]
        if compare_issue_wth_gitlab_issue(issue, gitlab_issue):
            return gitlab_issue_id
    return None


def match_gitlab_issue_with_sciit(gitlab_issue, issues):
    for issue_id in issues:
        issue = issues[issue_id]
        if compare_issue_wth_gitlab_issue(issue, gitlab_issue):
            return issue_id
    return None

