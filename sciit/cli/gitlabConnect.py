from sciit.gitlabConnect.repo import GitlabRepo
from git import Repo
from gitlab.exceptions import GitlabAuthenticationError, GitlabGetError
from sciit.repo import IssueRepo
from sciit.gitlabConnect.migrate import migrate
from sciit.gitlabConnect.functions import get_project_url
from sciit.gitlabConnect.update_source_code import update_issue_from_gitlab
from sciit.gitlabConnect.update_on_gitlabs import update_issue_on_gitlab
from sciit.gitlabConnect.gitlabCommunicator import GitlabCommunicator
from sciit.cli.color import ColorPrint


def gitlab_connect(args):
    git_repository = Repo(search_parent_directories=True)
    gitlab_repo = GitlabRepo(git_repository)
    if not gitlab_repo.is_init():
        gitlab_repo.setup_file_system_resources()
        ColorPrint.green("Gitlab Connection initiated")
    if args.reset:
        gitlab_repo.reset()
        gitlab_repo.setup_file_system_resources()
        ColorPrint.green("Gitlab Connection reset")
    if args.token:
        store_gitlab_token(args.token)
    if args.migrate:
        migrate(git_repository)
    if not gitlab_repo.is_functional():
        if not gitlab_repo.is_init():
            return
        else:
            ColorPrint.red(
                "The Gitlab token has not been registered, run \'git sciit gitlab-init [-t,--token] [YOUR_TOKEN]\'")
    else:
        ColorPrint.green("Gitlab Connection is functional")


def gitlab_action(args):
    git_repository = Repo(search_parent_directories=True)
    gitlab_repo = GitlabRepo(git_repository)
    if not gitlab_repo.is_init():
        gitlab_repo.print_uninitialized_error()
        return
    if not gitlab_repo.is_functional():
        ColorPrint.red(
            "The Gitlab token has not been registered, run \'git sciit gitlab-init [-t,--token] [YOUR_TOKEN]\'")
        return
    if args.action == 'migrate':
        migrate(git_repository)
    elif args.action == 'pull':
        issue_repository = IssueRepo(git_repository)
        issue_repository.cli = True
        open_issues = issue_repository.get_open_issues()
        update_issue_from_gitlab(git_repository, open_issues)
    elif args.action == 'push':
        issue_repository = IssueRepo(git_repository)
        issue_repository.cli = True
        update_issue_on_gitlab(git_repository, issue_repository.get_all_issues())
    elif args.action == 'disconnect':
        gitlab_repo.reset()
        ColorPrint.red("Gitlab connection has been disconnect")


def store_gitlab_token(gitlab_token):
    git_repository = Repo(search_parent_directories=True)
    gitlab_repo = GitlabRepo(git_repository)
    if not gitlab_repo.is_init():
        ColorPrint.red(
            "The Gitlab Connection have not been initialized run \'git sciit gitlab [-t,--token] [YOUR_TOKEN]\'")
    else:
        try:
            website_url, project_url = get_project_url(git_repository)
            GitlabCommunicator(website_url, project_url, gitlab_token)
            gitlab_repo.store_gitlab_token(gitlab_token)
            ColorPrint.green("Token saved")
        except GitlabAuthenticationError:
            ColorPrint.red("Entered Invalid Token")
        except GitlabGetError:
            ColorPrint.red("Not a Gitlab repository")
            gitlab_repo.reset()



