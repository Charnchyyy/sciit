# Installation

SCIIT required python3 installed on your local machine to work properly.
The machine default python should set to python3

Situation before installation:

    $ git sciit
    git: 'sciit' is not a git command. See 'git --help'.


Installation right from the source tree:

    $ python3 setup.py install
    $ python setup.py install (For Window)
    

Now, the `git sciit` command is available::

    $ git sciit

On Unix-like systems, the installation places a `git-sciit` script into a centralised `bin` directory, which should be 
in the `PATH` environment variable.

On Windows, `git-sciit.exe` is placed into a centralised `Scripts` directory which should also be in the `PATH`.
